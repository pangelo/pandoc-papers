
#
# edit these values to your liking
#

AUTHOR_SLUG=asmithee
PUBLICATION_SLUG=unconference2016

#
# there should be no need to edit anything below this line
#

OUTPUT_PREFIX=$(AUTHOR_SLUG)-$(PUBLICATION_SLUG)
OUTPUT_FILE=$(OUTPUT_PREFIX).pdf

SRC_FILES=$(OUTPUT_PREFIX)_main.tex \
	  $(OUTPUT_PREFIX)_src.tex

all:$(OUTPUT_FILE)

$(OUTPUT_FILE): $(SRC_FILES)
	pdflatex -jobname=$(OUTPUT_PREFIX) $<

%_src.tex: %_src.txt
	pandoc -t latex -o $@ $<

%.bbl: %.bib
	bibtex $(OUTPUT_PREFIX)

bibtex: $(OUTPUT_PREFIX).bbl

# this target tries to find the main .tex and .bib files and rename 
# them appropriately. It can go wrong in a variety of interesting ways

TEMPLATE_TEX=$(basename $(firstword $(wildcard *.tex)))
TEMPLATE_BIB=$(basename $(firstword $(wildcard *.bib)))
TEMPLATE_SRC=$(wildcard $(OUTPUT_PREFIX)_src.txt)

template:
ifeq ($(TEMPLATE_TEX),)
	touch $(OUTPUT_PREFIX)_main.tex
else ifneq ($(TEMPLATE_TEX), $(OUTPUT_PREFIX)_main)
	mv $(TEMPLATE_TEX).tex $(OUTPUT_PREFIX)_main.tex
endif
ifeq ($(TEMPLATE_BIB),)
	touch $(OUTPUT_PREFIX).bib
else ifneq ($(TEMPLATE_BIB), $(OUTPUT_PREFIX))
	mv $(TEMPLATE_BIB).bib $(OUTPUT_PREFIX).bib
endif
ifeq ($(TEMPLATE_SRC),)
	touch $(OUTPUT_PREFIX)_src.txt
endif

clean:
	-rm *~
	-rm $(OUTPUT_FILE)

distclean: clean
	-rm *.bbl
	-rm *.aux
	-rm *.out
	-rm *.log
	-rm *.blg
	-rm *.lot
	-rm *.lof
	-rm *.toc

preview:
	evince $(OUTPUT_FILE) &

.PHONY: template preview clean distclean
